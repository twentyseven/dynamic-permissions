package com.bitk;

import com.bitk.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.AntPathMatcher;

@SpringBootTest
class DemoApplicationTests {

	@Autowired
	UserMapper userMapper;


	@Test
	void contextLoads() {
		System.out.println(userMapper.getUserRolesById(1).toString());
//		System.out.println(userMapper.loadUserByUsername("1").toString());
	}

	//URL路径匹配工具--spring security自带
	AntPathMatcher antPathMatcher = new AntPathMatcher();

	@Test
	void testUrlMatch(){
		System.err.println(antPathMatcher.match("/test/**","/test/js"));
	}

}
