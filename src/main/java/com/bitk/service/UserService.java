package com.bitk.service;

import com.bitk.mapper.UserMapper;
import com.bitk.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    UserMapper userMapper;

    /**
     * 查询用户账号&密码&具有的角色
     *
     * @param username
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        //查询用户信息[账号、密码、个人信息等]
        User user = userMapper.loadUserByUsername(username);
        if (user==null)
            throw new UsernameNotFoundException("用户不存在！QAQ");
        //查询用户所具有的角色-并赋值
        user.setRoles(userMapper.getUserRolesById(user.getId()));
        return user;
    }
}
