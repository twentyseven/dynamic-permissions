package com.bitk.utils;


/**
 * 前后端的响应工具类
 *  RespBean.status
 *      200:业务处理成功
 *      500:业务处理失败
 */
public class RespBean {

    private Integer status;
    private String msg;
    private Object obj;

    /**
     * 这个支持链式编程
     * 然后把set方法的返回值设置为该类对象,
     * 返回this
     * @return
     */
    public static RespBean build(){
        return new RespBean();
    }


    /**
     * 请求成功函数
     * @param msg
     * @return
     */
    public static RespBean ok(String msg){
        return new RespBean(200,msg,null);
    }

    public static RespBean ok(String msg,Object obj){
        return new RespBean(200,msg,obj);
    }

    /**
     * 请求失败
     * @param msg
     * @return
     */
    public static RespBean error(String msg){
        return new RespBean(500,msg,null);
    }

    public static RespBean error(String msg,Object obj){
        return new RespBean(500,msg,obj);
    }

    public RespBean() {
    }

    public RespBean(Integer status, String msg, Object obj) {
        this.status = status;
        this.msg = msg;
        this.obj = obj;
    }

    public Integer getStatus() {
        return status;
    }

    public RespBean setStatus(Integer status) {
        this.status = status;
        return this;
    }

    public String getMsg() {
        return msg;
    }

    public RespBean setMsg(String msg) {
        this.msg = msg;
        return this;
    }

    public Object getObj() {
        return obj;
    }

    public RespBean setObj(Object obj) {
        this.obj = obj;
        return this;
    }

}