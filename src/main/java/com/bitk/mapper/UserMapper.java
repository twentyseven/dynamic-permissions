package com.bitk.mapper;


import com.bitk.model.Role;
import com.bitk.model.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface UserMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    User selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);

    //根据用户名查询用户信息
    @Select("select * from user where username=#{username}")
    User loadUserByUsername(String username);

    List<Role> getUserRolesById(Integer id);
}
